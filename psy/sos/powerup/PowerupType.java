package psy.sos.powerup;

/**
 * all the powerup types
 * @author eli
 *
 */
public enum PowerupType {

	Nitrous("", Duration.Long), /* since this is for all cars, perhaps this shouldnt be considered a powerup */
	Juggernaut("", Duration.Long),
	Slingshot("", Duration.Long),
	Shield("", Duration.Long),
	
	/** NFS powerups **/
	
	WindRealm("", Duration.Long),
	SolarRealm("", Duration.Long),
	BlizzardRealm("", Duration.Long),
	MicroRealm("", Duration.Long),
	FogRealm("", Duration.Long),
	WarpedRealm("", Duration.Long),
	MetroRealm("", Duration.Long),
	IceRealm("", Duration.Long),
	RuinsRealm("", Duration.Long),
	MonumentRealm("", Duration.Long);
	
	String info;
	
	Duration duration;
	
	PowerupType(String info, Duration duration) {
		this.info = info;
		this.duration = duration;
	}
	
	public String getInfo() {
		return info;
	}
	
	public Duration getDuration() {
		return duration;
	}
	
	/**
	 * powerup duration
	 * @author eli
	 *
	 */
	public enum Duration {
		Long(250), Short(150), Custom(0);
		
		public int time;
		
		Duration(int time) {
			this.time = time;
		}
		
		public int getTime() {
			return time;
		}
	};
}
