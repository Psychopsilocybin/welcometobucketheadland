package psy.sos.music;


import java.io.ByteArrayInputStream;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;

import psy.sos.GameSparker;

/**
 * The Class SoundClip.
 */
public class SoundClip {

    private Clip clip = null;
    private int cntcheck = 0;
    private int lfrpo = -1;
    private boolean loaded = false;
    // Removed unused code found by UCDetector

    // 	int rollBackPos;
    int rollBackTrig;

    private AudioInputStream sound;

    /**
     * Instantiates a new sound clip.
     *
     * @param is the is
     */
    public SoundClip(final byte[] is) {
        try {
            final ByteArrayInputStream bytearrayinputstream = new ByteArrayInputStream(is);
            sound = AudioSystem.getAudioInputStream(bytearrayinputstream);
            sound.mark(is.length);
            clip = AudioSystem.getClip();
            loaded = true;
        } catch (final Exception exception) {
            if (GameSparker.DEBUG) {
                System.out.println("Loading Clip error: " + exception);
            }
            loaded = false;
        }
    }

    /**
     * Checkopen.
     */
    public void checkopen() {
        if (loaded && clip.isOpen() && lfrpo != -2)
            if (cntcheck == 0) {
                final int i = clip.getFramePosition();
                if (lfrpo == i && !clip.isRunning()) {
                    try {
                        clip.close();
                        sound.reset();
                    } catch (final Exception exception) {

                    }
                    lfrpo = -1;
                } else {
                    lfrpo = i;
                }
            } else {
                cntcheck--;
            }
    }

    /**
     * Loop.
     */
    public void loop() {
        if (loaded) {
            if (!clip.isOpen()) {
                try {
                    clip.open(sound);
                } catch (final Exception exception) {

                }
            }
            clip.loop(Clip.LOOP_CONTINUOUSLY);
            lfrpo = -2;
            cntcheck = 0;
        }
    }

    /**
     * Play.
     */
    public void play() {
        if (loaded) {
            if (!clip.isOpen()) {
                try {
                    clip.open(sound);
                } catch (final Exception exception) {

                }
                clip.loop(0);
            } else {
                clip.loop(1);
            }
            lfrpo = -1;
            cntcheck = 5;
        }
    }

    /**
     * Stop.
     */
    public void stop() {
        if (loaded) {
            clip.stop();
            lfrpo = -1;
        }
    }

    public void setGain(final float gain) {
        if (loaded) {
            if (!clip.isOpen()) {
                try {
                    clip.open(sound);
                } catch (final Exception exception) {

                }
            }
            final FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
            gainControl.setValue(gain);
        }
    }
}
