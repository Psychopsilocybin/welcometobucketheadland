package psy.sos.powerup;

public class Powerup {
	
	/**
	 * the type of this powerup
	 */
	PowerupType powertype;
	
	/**
	 * true upon user action
	 */
	public boolean useIt = false;
	
	/**
	 * the time after wearing off and being available again
	 */
	public boolean aftermath = false;
	
	/**
	 * how long the powerup takes to regenerate
	 */
	public int regenerationDelay = 450;
	
	public int regenerationTimer = 0;
	
	/**
	 * duration once it's used 
	 */
	public int effectDuration = 250;
	
	public boolean customDuration = false;
	
	public int effectDurationTimer = 0;
	
	public Powerup(PowerupType powertype) {
		this.powertype = powertype;
		this.effectDuration = powertype.getDuration().getTime();
	}
	
	public PowerupType getType() {
		return powertype;
	}
	
	/**
	 * is the powerup wore off
	 * @return true or false
	 */
	public boolean readyToWearOff() {
		if(useIt) {
			if(effectDuration != 0) {
				if(effectDurationTimer < effectDuration){
					effectDurationTimer++;
				}else{
					effectDurationTimer = 0;
					aftermath = true;
					return true;
				}
			} else {
				if(customDuration) {
					customDuration = false;
					aftermath = true;
					return true;
				}
			}
			
		}
		return false;
	}
	
	/**
	 * is the powerup ready to be available again
	 * @return true or false
	 */
	public boolean readyToRegenerate() {
		if(aftermath) {
			if(regenerationTimer < regenerationDelay) {
				regenerationTimer++;
			}else{
				regenerationTimer = 0;
				aftermath = false;
				return true;
			}
		}		
		return false;
	}

}
