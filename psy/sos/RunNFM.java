
package psy.sos;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import psy.sos.music.TrackZipLoader;

public class RunNFM extends Panel {		
	/**
	 * 
	 */
	private static final long serialVersionUID = -8590687589434803725L;
	
	private static JFrame frame;
	private static JComponent applet;
	public static ArrayList<Image> icons;
	
	public static String RUNPATH = "";
	

	/**
	 * Fetches icons of 16, 32 and 48 pixels from the 'data' folder.
	 * @return icons - ArrayList of icon locations
	 */
	public static ArrayList<Image> getIcons() {
		if (icons == null) {
			icons = new ArrayList<Image>();
			int[] resols = {
					16, 32, 48
			};
			for (int res : resols) {
				icons.add(Toolkit.getDefaultToolkit().createImage("data/ico_" + res + ".png"));
			}
		}
		return icons;
	}

	public static void main(String[] strings) {		
		System.out.println("NFM:SOS Console");
		try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e1) {
            e1.printStackTrace();
        }
		
		frame = new JFrame("NFM:SOS");
		frame.setBackground(Color.BLACK);
		frame.setIgnoreRepaint(true);
		
		RUNPATH = "";
		boolean bool = false;
        for (final String arg : strings) {
            if (!bool) {
            	RUNPATH = RUNPATH + arg;
                bool = true;
            } else {
            	RUNPATH = RUNPATH + " " + arg;
            }
        }
        if (!RUNPATH.equals("") && !RUNPATH.endsWith("/") && !RUNPATH.endsWith("\\")) {
        	RUNPATH = RUNPATH + "/";
        }
		
        frame.setIconImages(getIcons());
		
		applet = new GameSparker();			
		
		Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                /*if (Cheat.canSaveSettings)
                    Settings.saveSettings(xtGraphics.credits, xtGraphics.unlocked, xtGraphics.sc[0]);*/                
                try {
                    TrackZipLoader.deletetempfiles();
                } catch (final Exception e) {                   
                    System.out.println("finalization error:");                    
                    e.printStackTrace();
                }
                System.runFinalization();
            }
        });
		
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent windowevent) {
				exitSequence();
			}
		});
		
		applet.setPreferredSize(new Dimension(670, 400));// The resolution of your game goes here
		frame.add("Center", applet);
		frame.setResizable(false);// If you plan to make you game support changes in resolution, you can comment out this line.
		frame.pack();
		frame.setMinimumSize(frame.getSize());
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public static void exitSequence() {
		frame.removeAll();
		try {
			Thread.sleep(200L);
		} catch (Exception exception) {
		}
		System.exit(0);
	}
}