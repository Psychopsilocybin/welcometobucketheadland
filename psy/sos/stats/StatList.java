package psy.sos.stats;

/**
 * the stat class
 * @author eli
 *
 */
public class StatList {
	
	/**
	 * The Acceleration stat
	 * first value being how fast the car can move from rest (not moving). 
	 * Second and third value meaning how much speed it gains while the car is moving
	 * @author Omar Waly
	 */
	public static float acelf[][] = {
			{
					11F, 5F, 3F
			}, {
					14F, 7F, 5F
			}, {
					10F, 5F, 3.5F
			}, {
					11F, 6F, 3.5F
			}, {
					10F, 5F, 3.5F
			}, {
					12F, 6F, 3F
			}, {
					7F, 9F, 4F
			}, {
					11F, 5F, 3F
			}, {
					12F, 7F, 4F
			}, {
					12F, 7F, 3.5F
			}, {
					11.5F, 6.5F, 3.5F
			}, {
					9F, 5F, 3F
			}, {
					13F, 7F, 4.5F
			}, {
					7.5F, 3.5F, 3F
			}, {
					11F, 7.5F, 4F
			}, {
					12F, 6F, 3.5F
			}/* 20 new cars */, {
					14F, 7F, 5F
			}, {
					2F, 4F, 5F
			}, {
					2F, 4F, 5F
			}, {
					2F, 4F, 5F
			}, {
					2F, 4F, 5F
			}, {
					2F, 4F, 5F
			}, {
					2F, 4F, 5F
			}, {
					2F, 4F, 5F
			}, {
					2F, 4F, 5F
			}, {
					2F, 4F, 5F
			}, {
					2F, 4F, 5F
			}, {
					2F, 4F, 5F
			}, {
					2F, 4F, 5F
			}, {
					2F, 4F, 5F
			}, {
					2F, 4F, 5F
			}, {
					2F, 4F, 5F
			}, {
					2F, 4F, 5F
			}, {
					2F, 4F, 5F
			}, {
					2F, 4F, 5F
			}, {
					2F, 4F, 5F
			}
	};
	/**
	 * The Top Speed stat
	 * Take the values as if each one was a gear from a gearbox. 1st to 2nd, 2nd to 3rd, dependent on power obtained
	 * @author Omar Waly
	 */
	public static int swits[][] = {
			{
					50, 180, 280
			}, {
					100, 200, 310
			}, {
					60, 180, 275
			}, {
					70, 190, 295
			}, {
					70, 170, 275
			}, {
					60, 200, 290
			}, {
					60, 170, 280
			}, {
					60, 180, 280
			}, {
					90, 210, 295
			}, {
					90, 190, 276
			}, {
					70, 200, 295
			}, {
					50, 160, 270
			}, {
					90, 200, 305
			}, {
					50, 130, 210
			}, {
					80, 200, 300
			}, {
					70, 210, 290
			}/*20 new cars*/, {
				    85, 205, 305
			}, {
					20, 40, 50
			}, {
					20, 40, 50
			}, {
					20, 40, 50
			}, {
					20, 40, 50
			}, {
					20, 40, 50
			}, {
					20, 40, 50
			}, {
					20, 40, 50
			}, {
					20, 40, 50
			}, {
					20, 40, 50
			}, {
					20, 40, 50
			}, {
					20, 40, 50
			}, {
					20, 40, 50
			}, {
					20, 40, 50
			}, {
					20, 40, 50
			}, {
					20, 40, 50
			}, {
					20, 40, 50
			}, {
					20, 40, 50
			}, {
					20, 40, 50
			}, {
					20, 40, 50
			}
	};
	/**
	 * Handbrake power
	 * Basically, the braking power. Any negative value, would make you shoot forward instead of backward
	 * @author Omar Waly
	 */
	public static int handb[] = {
			7, 10, 7, 15, 12, 8, 9, 10, 5, 7, 8, 10, 8, 12, 7, 7 /* 20 new cars */, 14, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	};
	/**
	 * Aerial rotation
	 * If the value is too much, your car will just rotate rapidly and you can't control it.
	 * @author Omar Waly
	 */
	public static float airs[] = {
			1.0F, 1.2F, 0.95F, 1.0F, 2.2F, 1.0F, 0.9F, 0.8F, 1.0F, 0.9F, 1.15F, 0.8F, 1.0F, 0.3F, 1.3F, 1.0F/* 20 new cars */, 1.1F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F
	};
	/**
	 * Aerial control
	 * This controls how high or low you'll go if you do flips, or how far you'll go if you do rollspins. 
	 * This also effects the statbar of Aerial Control, but if this value is noticeably low, then the airs 
	 * value will be used to display how much Aerial Control a car has.
	 * @author Omar Waly
	 */
	public static int airc[] = {
			70, 30, 40, 40, 30, 50, 40, 90, 40, 50, 75, 10, 50, 0, 100, 60/*20newcars*/, 85, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40
	};
	/**
	 * Turning responsiveness
	 * @author Omar Waly
	 */
	public static int turn[] = {
			6, 9, 5, 7, 8, 7, 5, 5, 9, 7, 7, 4, 6, 5, 7, 6/*20 new*/, 10, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	};
	/**
	 * Grip of the car to the ground
	 * @author Omar Waly
	 */
	public static float grip[] = {
			20F, 27F, 18F, 22F, 19F, 20F, 25F, 20F, 16F, 24F, 22.5F, 25F, 30F, 27F, 25F, 27F/*20new cars*/, 28F, 18F, 18F, 18F, 18F, 18F, 18F, 18F, 18F, 18F, 18F, 18F, 18F, 18F, 18F, 18F, 18F, 18F, 18F, 18F
	};
	/**
	 * How much your car bounces after landing stunts
	 * @author Omar Waly
	 */
	public static float bounce[] = {
			1.2F, 1.05F, 1.3F, 1.15F, 1.3F, 1.2F, 1.15F, 1.1F, 1.2F, 1.1F, 1.15F, 0.8F, 1.05F, 0.8F, 1.1F, 1.15F/*20new car*/, 1.0F, 1.3F, 1.3F, 1.3F, 1.3F, 1.3F, 1.3F, 1.3F, 1.3F, 1.3F, 1.3F, 1.3F, 1.3F, 1.3F, 1.3F, 1.3F, 1.3F, 1.3F, 1.3F, 1.3F
	};
	/**
	 * Tolerance towards track pieces
	 * @author Omar Waly
	 */
	public static float simag[] = {
			0.9F, 0.85F, 1.05F, 0.9F, 0.85F, 0.9F, 1.05F, 0.9F, 1.0F, 1.05F, 0.9F, 1.1F, 0.9F, 1.3F, 0.9F, 1.15F/*20new*/, 0.9F, 0.85F, 0.85F, 0.85F, 0.85F, 0.85F, 0.85F, 0.85F, 0.85F, 0.85F, 0.85F, 0.85F, 0.85F, 0.85F, 0.85F, 0.85F, 0.85F, 0.85F, 0.85F, 0.85F
	};
	/**
	 * Car strength
	 * @author Omar Waly
	 */
	public static float moment[] = {
			1.2F, 0.75F, 1.4F, 1.0F, 0.85F, 1.25F, 1.4F, 1.3F, 1.2F, 1.45F, 1.375F, 2.0F, 1.2F, 3F, 1.5F, 2.0F/*20new*/, 1.75F, 1.4F, 1.4F, 1.4F, 1.4F, 1.4F, 1.4F, 1.4F, 1.4F, 1.4F, 1.4F, 1.4F, 1.4F, 1.4F, 1.4F, 1.4F, 1.4F, 1.4F, 1.4F, 1.4F
	};
	/**
	 * Tolerance towards other cars
	 * @author Omar Waly
	 */
	public static float comprad[] = {
			0.5F, 0.4F, 0.8F, 0.5F, 0.3F, 0.5F, 0.5F, 0.5F, 0.5F, 0.8F, 0.5F, 1.0F, 0.5F, 0.6F, 0.5F, 0.8F/*20new*/, 0.7F, 0.5F, 0.5F, 0.5F, 0.5F, 0.5F, 0.5F, 0.5F, 0.5F, 0.5F, 0.5F, 0.5F, 0.5F, 0.5F, 0.5F, 0.5F, 0.5F, 0.5F, 0.5F, 0.5F
	};
	/**
	 * How much a car can push another car while wasting
	 * @author Omar Waly
	 */
	public static int push[] = {
			2, 2, 3, 3, 2, 2, 2, 4, 2, 2, 2, 4, 2, 2, 2, 2/*20new*/, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
	};
	/**
	 * The amount of "reverse push" or recoil the car will get from others
	 * @author Omar Waly
	 */
	public static int revpush[] = {
			2, 3, 2, 2, 2, 2, 2, 1, 2, 1, 2, 1, 2, 2, 2, 1/*20new*/, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
	};
	/**
	 * How much a car can lift another car while wasting
	 * @author Omar Waly
	 */
	public static int lift[] = {
			0, 30, 0, 20, 0, 30, 0, 0, 20, 0, 0, 0, 10, 0, 30, 0/*20new*/, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
	};
	/**
	 * The amount of "reverse lift" or recoil the car will get from others
	 * @author Omar Waly
	 */
	public static int revlift[] = {
			0, 0, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 32/*20new*/, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
	};
	/**
	 * The variable for power loss, higher means longer power duration. Value in Hexadecimal.
	 * @author Omar Waly
	 */
	public static int powerloss[] = {
			0x2625a0, 0x2625a0, 0x3567e0, 0x2625a0, 0x3d0900, 0x2625a0, 0x30d400, 0x30d400, 0x29f630, 0x53ec60,
			0x29f630, 0x44aa20, 0x3567e0, 0xfed260, 0x2dc6c0, 0x53ec60 /*20new*/, 0x53ec60, 0x2625a0, 0x2625a0, 0x2625a0, 0x2625a0, 0x2625a0, 0x2625a0, 0x2625a0, 0x2625a0, 0x2625a0, 0x2625a0, 0x2625a0, 0x2625a0, 0x2625a0, 0x2625a0, 0x2625a0, 0x2625a0, 0x2625a0, 0x2625a0, 0x2625a0
	};
	/**
	 * Y value of cars when flipped over during a bad landing
	 * @author Omar Waly
	 */
	public static int flipy[] = {
			-50, -60, -92, -44, -60, -57, -54, -60, -77, -57, -82, -85, -28, -100, -63, -127/*20new*/, -65, -44, -44, -44, -44, -44, -44, -44, -44, -44, -44, -44, -44, -44, -44, -44, -44, -44, -44, -44
	};
	/**
	 * How badly a car can get smashed during wasting
	 * @author Omar Waly
	 */
	public static int msquash[] = {
			7, 4, 7, 2, 8, 4, 6, 4, 3, 8, 4, 10, 3, 20, 3, 8/*20new*/, 6, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4
	};
	/**
	 * Collision radius
	 * 
	 * "Clrad is the radius around each point that gets collided..
	 * So if you have a car with so much points, you need the clrad 
	 * to be low or it will get damaged very fast as so many points 
	 * will get effected...If a car has low points you need the clrad 
	 * to be bigger so the damage reaches other points or else the car 
	 * will be indestructible..."
	 * @author Omar Waly
	 */
	public static int clrad[] = {
			3300, 3000, 4700, 3000, 2000, 4500, 3500, 5000, 20000, 15000, 4000, 7000, 10000, 30000, 5500, 5000/*20new*/, 1650, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000
	};
	/**
	 * Damage Multiplier 
	 * "The amount of damage based on the hit that should effect the points in clrad around the point of collision..."
	 * @author Omar Waly
	 */
	public static float dammult[] = {
			0.8F, 1.17F, 0.55F, 1.0F, 0.6F, 0.7F, 0.72F, 0.8F, 0.6F, 0.46F, 0.67F, 0.5F, 0.61F, 0.176F, 0.36F, 0.46F/*20new*/, 0.61F, 0.46F, 0.46F, 0.46F, 0.46F, 0.46F, 0.46F, 0.46F, 0.46F, 0.46F, 0.46F, 0.46F, 0.46F, 0.46F, 0.46F, 0.46F, 0.46F, 0.46F, 0.46F, 0.46F
	};
	/**
	 * The amount of Damage that a car can take before it is considered as "wasted".
	 * @author Omar Waly
	 */
	public static int maxmag[] = {
			6000, 4200, 7200, 6000, 6000, 9100, 14000, 12000, 12000, 9700, 13000, 10700, 13000, 30000, 5800, 18000/*20new*/, 12000, 4200, 4200, 4200, 4200, 4200, 4200, 4200, 4200, 4200, 4200, 4200, 4200, 4200, 4200, 4200, 4200, 4200, 4200, 4200
	};
	
	public static float dishandle[] = {
			0.65F, 0.6F, 0.55F, 0.77F, 0.62F, 0.9F, 0.6F, 0.72F, 0.45F, 0.8F, 0.95F, 0.4F, 0.87F, 0.42F, 1.0F, 0.95F/*20new*/, 0.83F, 0.6F, 0.6F, 0.6F, 0.6F, 0.6F, 0.6F, 0.6F, 0.6F, 0.6F, 0.6F, 0.6F, 0.6F, 0.6F, 0.6F, 0.6F, 0.6F, 0.6F, 0.6F, 0.6F
	};
	
	public static float outdam[] = {
			0.67F, 0.35F, 0.8F, 0.5F, 0.42F, 0.76F, 0.82F, 0.76F, 0.72F, 0.62F, 0.79F, 0.95F, 0.77F, 1.0F, 0.85F, 1.0F/*20new*/, 0.76F, 0.76F, 0.76F, 0.76F, 0.76F, 0.76F, 0.76F, 0.76F, 0.76F, 0.76F, 0.76F, 0.76F, 0.76F, 0.76F, 0.76F, 0.76F, 0.76F, 0.76F, 0.76F, 0.76F
	};
	
	public static String names[] = {
			"Tornado Shark", "RD25", "Wow Caninaro", "La Vite Crab", "OSD", "MAX Revenge", "Lead Oxide",
			"Kool Kat", "DX", "Sword of Justice", "High Rider", "EL KING", "The Diablo", "M A S H E E N",
			"Radical One", "4x4 Rover"/*20new*/,"F1", "Prototype Coupe", "C8",  "Takata Dome", "RX-7 PSY",  "New Car", "New Car", "New Car", "New Car", "New Car", "New Car", "New Car", "New Car", "New Car", "New Car", "New Car", "New Car", "New Car", "New Car", "New Car", "New Car", "New Car"
	};

}
